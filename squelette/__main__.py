#!/usr/bin/env python3

# Copyright Louis Paternault 2011-2024
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Add numbers"""

import argparse
import gettext
import importlib.resources
import logging
import sys

import squelette

LOGGER = logging.getLogger(squelette.__name__)
LOGGER.addHandler(logging.StreamHandler())

# Install gettext()
with importlib.resources.as_file(
    importlib.resources.files(__package__) / "translations"
) as localedir:
    gettext.install("squelette", localedir)


def commandline_parser():
    """Return a command line parser."""

    parser = argparse.ArgumentParser(
        prog="squelette",
        description=_("Python package template"),
        epilog=_("As a template, this program sums numbers given in argument."),
    )

    parser.add_argument(
        "--version",
        help=_("Show version"),
        action="version",
        version="%(prog)s " + squelette.VERSION,
    )

    parser.add_argument(
        "NUMBER", help=_("List of numbers to add"), nargs="*", type=float
    )

    return parser


def main():
    """Main function"""
    arguments = commandline_parser().parse_args()
    print(_("The sum of the arguments is %d.") % squelette.add(*arguments.NUMBER))
    sys.exit(0)


if __name__ == "__main__":
    main()
