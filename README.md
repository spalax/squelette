# squelette ☠ Python3 package template

This package contains the common stuff of my python packages : tests, documentation, basic program structure, etc. All of this is described in the [documentation](https://squelette.readthedocs.io/en/latest).

## What's new?

See [changelog](https://git.framasoft.org/spalax/squelette/blob/main/CHANGELOG.md)

## Download and install

* From sources:

  * Download: https://pypi.python.org/pypi/squelette
  * Install (in a `virtualenv`, if you do not want to mess with your distribution installation system):

          python3 -m pip install .

* From pip:

      pip install squelette

* Quick and dirty Debian (and Ubuntu?) package

  This requires [stdeb](https://github.com/astraw/stdeb) to be installed:

        python3 setup.py --command-packages=stdeb.command bdist_deb
        sudo dpkg -i deb_dist/squelette-<VERSION>_all.deb

## Documentation

* The compiled documentation is available on [readthedocs](http://squelette.readthedocs.io)

* To compile it from source, download and run:

        cd doc && make html
