Release process
===============

- Compile translations::

    pybabel compile -d <package>/translations --domain <domain>

- Tests

  - Test with current python version::

      python -m unittest

  - Test with all supported versions::

      tox

- Bump version number AND copyright year

  - `setup.cfg`
  - `<program>/__init__.py`
  - `doc/conf.py`
  - `CHANGELOG.md`
  - be sure not to forget anything::

      git grep <current_version>
      git grep 20[0-9][0-9]

- Test publishing:

  - Build::

      rm -fr build dist
      python3 -m build

  - Documentation should build: `https://readthedocs.io/builds/<program>/`
  - Check that README compiles flawlessly

    - with package `restructuredtext-lint`::

        rst-lint README.rst

    - with package `readme_renderer`::

        twine check dist/*

  - README should be correctly rendered: `https://git.framasoft.org/spalax/<program>`

- Commit new versions

  - Update changelog
  - Commit::

      git commit -m "Version <version>"
      git tag v<version>

- Publish:

  - Re-read and redact README for stuff that should not appear in Pypi.

  - Build again::

      rm -fr build dist
      python -m build

  - Git::

      git push
      git push --tags

  - Pypi: Register if necessary::

      twine register dist/<program>-<version>.tar.gz

  - Pypi: Publish::

      twine upload dist/<program>-<version>*
