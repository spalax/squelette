#!/usr/bin/env python3

# Copyright Louis Paternault 2011-2022
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 1

"""Test of calendar parser and renderer"""
# pylint: disable=too-few-public-methods

import glob
import os
import unittest

import squelette
from squelette.__main__ import commandline_parser


class TestAdd(unittest.TestCase):
    """Test core :func:`squelette.add` function."""

    def test_add(self):
        """Test add function, with several parameters."""
        dirname = os.path.dirname(__file__)
        files = glob.glob(os.path.join(dirname, "*.in"))
        for infile in [os.path.basename(file) for file in sorted(files)]:
            basename = infile[: -len(".in")]
            outfile = f"{basename}.out"
            with self.subTest(
                msg=f"Testing '{infile}'",
                dirname=dirname,
                infile=infile,
                outfile=outfile,
            ):  # pylint: disable=line-too-long
                with open(os.path.join(dirname, infile), encoding="utf8") as sourcefile:
                    with open(
                        os.path.join(dirname, outfile), encoding="utf8"
                    ) as expectfile:
                        self.assertEqual(
                            squelette.add(
                                *commandline_parser()
                                .parse_args(sourcefile.read().split())
                                .NUMBER
                            ),  # pylint: disable=line-too-long
                            float(expectfile.read()),
                        )
