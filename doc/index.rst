=====================================
Welcome to squelette's documentation!
=====================================

This package is a template package for my new python projects.

Features
========

Binary
------

* There are several ways to call the binary.

  * The package contains a ``__main__`` module, so you can use ``python -m squelette [ARGS]``.
  * There is an entry point in the setup file. That way, the binary is
    correctly installed, whatever the platform, and once installed, you can use
    ``squelette [ARGS]``.
  * In the ``bin`` directory, not shipped when distributing the program, there
    is a ``squelette`` binary. In development environment, you can use
    ``./bin/squelette [ARGS]``.
  * The package is translated using `gettext <https://docs.python.org/3/library/gettext.html>`_.

* My usual :mod:`logging` configuration is set.

.. _test:

Tests
-----

Several parts are tested; everything is configured in :download:`tox.ini
<../tox.ini>`.

  * First of all, `tox <http://codespeak.net/tox/>`__ is used to aggregate all
    of the following tests. To perform all the tests (with different python
    versions), in the root project directory, run:

    .. code-block:: sh

      $ tox

  * To run :mod:`unittest`, in
    the root directory, run:

    .. code-block:: sh

      $ python -m unittest

  * Unittest call also run :mod:`doctest` present accross modules.
    Thus, you can freely add doctests anywhere in your program, and they will
    be performed along with unittests or tox. This part is performed in
    :download:`squelette/test.py <../test/test_doctest.py>`.

  * It can be a bit cumbersome to add tests for a program or library acting on
    files. File :download:`test/test_add/__init__.py
    <../test/test_add/__init__.py>` scans module directories for couples
    of ``.in`` and ``.out``, and creates one :class:`unittest.TestCase` per
    couple. Using one test case per couple, when a test fails, it is easier to
    track which couple of files caused the problem than if tests where
    performed in a loop in a single test case.

    Note that for simple cases (like this one), this could also have been
    achieved by using :meth:`unittest.TestCase.subTest`.

  * Tox also checks that :ref:`pylint` runs without any error.
  * Tox also checks that :ref:`documentation` is compiled without
    any warning or error.

Release and Distribution
------------------------

  * `Hatch <https://hatch.pypa.io/>`_ is used with :download:`pyproject.toml <../pyproject.toml>` file, with my usual configuration. Among
    others:

    * The :download:`README.md <../README.md>` file content is used as the long description (don't repeat yourself).
    * Entry points are used, so that binaries are automatically installed the right way, disregarding specific platform.
  * My usual release process is described in file :download:`release.rst <../release.rst>`.
  * `Stdeb <https://github.com/astraw/stdeb>`__ is used to build quick and dirty debian packages. It is configured in :download:`stdeb.cfg <../stdeb.cfg>`.
  * To access packages data, no matter what release are, they are placed in package sub directories, and accessed using `importlib.resources <https://docs.python.org/3/library/importlib.resources.html>`_. An example is available in file :download:`squelette/__main__.py <../squelette/__main__.py>`.
  * A `build hook <https://hatch.pypa.io/latest/plugins/build-hook/reference/>`__ is used to automatically compile gettext message catalogs while building the python package: :download:`hatch_build_babel.py <../tools/hatch_build_babel.py>`.

.. _documentation:

Documentation
-------------

  * My usual :download:`README.rst <../README.rst>` is provided, with links, basic documentation, and
    badges.
  * The documentation is built using `sphinx <http://sphinx-doc.org/>`_.
  * Sphinx compilation is tested by :ref:`tox <test>`.
  * `Readthedocs <http://squelette.rtfd.io>`__ is configured using :download:`a YAML file <../.readthedocs.yaml>`.

.. _pylint:

Code formatting
---------------

  * The code is `Pylint <http://pylint.org>`__ compliant. Configuration is done
    in the :download:`pylintrc <../pylintrc>` file.
  * The code is formatted using `black <http://https://github.com/ambv/black>`__. If `pre-commit <https://pre-commit.com>`__ is properly installed and configured (i.e. ``pip install pre-commit`` to install, and ``pre-commit install`` to set up pre-commit hooks), black is automatically run each time you ``git commit``.
  * :ref:`Tox <test>` tests that pylint and black raises no error.


Download and install
====================

See the `main project page <http://git.framasoft.org/spalax/squelette>`_ for
instructions, and `changelog
<https://git.framasoft.org/spalax/squelette/blob/main/CHANGELOG.md>`_.

Usage
=====

Here are the command line options for `squelette`.

.. argparse::
    :module: squelette.__main__
    :func: commandline_parser
    :prog: squelette
